# Help &amp; Support

If you need any assistance with this extension, the iSenseLabs support team will be more than happy to assist. Just open up a support ticket in https://isenselabs.com/tickets/open and we will do our best to respond in a timely manner!

For sales-related questions or new feature requests, please contact us on <a href="mailto:sales@isenselabs.com">sales@isenselabs.com</a>