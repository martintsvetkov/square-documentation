# CRON Job Settings

The Square extension needs a daily CRON job which carries out recurring payments and automatically refreshes the connection to your Square application. To remind you that recurring payments will not function correctly without a CRON job, the extension requires you to check the **Setup confirmation** checkbox once you have configured the CRON job. Set up a daily CRON task using one of the methods below.

### Method 1 - Using a CRON job

A CRON job runs periodically on your server to perform scheduled operations like recurring payments. More information on CRON jobs can be found here:

http://en.wikipedia.org/wiki/Cron

You can setup a daily CRON job in the administration panel of your hosting provider.  You will have to provide a command to be executed, which you can copy from the field **Method #1 - Cron Task** in the **Recurring Payments** section of the administration settings of the Square extension:

<img src="/doc/squareup/img/cron_method1.jpg" alt="cron_method1_field">


### Method 2 - Using a remote CRON service

A remote CRON service periodically visits a URL on your site which allows you to execute scheduled operations when you cannot setup a local CRON job. You will have to provide that service with the URL from the **Method 2 - Remote CRON** field in the **Recurring Payments** section of the administaration settings of the Square extension. A security token is added to the URL to prevent unauthorized access. If you regenerate the security token using the refresh icon next to that field, make sure you save the settings before using the new URL.

<img src="/doc/squareup/img/cron_method2.jpg" alt="cron_method2_field">

### Admin notifications

You can enable an email notification with a summary of all taks performed by the CRON job using the **Send email summary** dropdown in the **Admin notifications** settings. By default these emails will go to the OpenCart administrator email, but you can configure a different one in the **Send task summary to this e-mail** field.