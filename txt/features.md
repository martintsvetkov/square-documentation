# Features and perks

### Main features

Square extension for OpenCart supports the following major features:

- Tokenzation of credit cards - avoid risks of storing credit card data on your servers, expensive breaches, and ongoing PCI compliance costs. Card data is securely transmitted directly to Square rather than passing through your server, reducing the surface area of hacking by taking you and your server entirely out of PCI scope.
- Card on File - Cards can be saved to speed up all subsequent checkouts done by your customers, leading to better customer experience. Customers with tokenized credit cards can make fast checkouts without getting redirected and entering card details.
- Supports both 1-stage (Authorization + Immediate Capture) and 2-stage transaction (Authorize + Delayed Capture).
- Voiding and Refunding transactions directly from the OpenCart admin panel.
- Use a Test mode within a special Sandbox environment inside Square to ensure extension is properly set up.
- Detailed debug logging - indispensable for developers and support experts who want to take a closer look.
- Detailed display of transaction data including: Transaction ID, Order ID, Status, Transaction Result, Transaction Type, Amount, Customer User Agent, Customer IP and more.
- Automatic recurring payments and credential updates via the the cron job script, included for free with the extension.

### Extras

You benefit from Square's simplified eCommerce platform of

- 100% free. The extension is shipped free of charge.
- 100% mobile friendly. The admin panel looks great on desktop and mobile views.
- Security-oriented. The extension works only on stores with SSL. No sensitive data is stored on your site - payments, fraud checks, storing of credit card information are managed in a secure and standardized fashion directly by Square.
- Seamless integration into OpenCart. The extension hooks to your OpenCart store without overriding any core files, leaving a very small footprint.
- Tidy and standardized code. The extension is developed with the best coding practices in mind.
- Constant updates. The iSenseLabs developers always do their best to give you the most up-to-date release of the extension with all known bugs fixed.
- Clean and crisp documentation.
- Quick support. Have a question? Our friendly support team will do their best to provide a solution in a timely manner.
- This extension has been officially endorsed by Square.