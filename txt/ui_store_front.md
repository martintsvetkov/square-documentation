# User Interface: Store Front

This is what your customers will see at the final stage of the checkout if they do not have any associated cards. In the example here we are running the extension in test mode which is why the yellow notification is showing up.

<img src="/doc/squareup/img/checkout.jpg" alt="checkout">

On the other hand, if a customer has already made a checkout with a valid credit card and has chosen to save their card, they can select it to skip manual entry. This is what they will see on their second and subsequent checkouts:

<img src="/doc/squareup/img/checkout_tokenized.jpg" alt="checkout_tokenized">

Upon order confirmation the transaction will be authorized or captured according to the extension settings using the already stored on Square card token.