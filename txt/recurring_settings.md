# Recurring Payment Settings

You can enable recurring payments in the **Recurring Payments** tab of the administration settings for the Square extension using the **Status of recurring payments** dropdown.
Recurring transactions must be carried out by your server, so you need to setup a daily CRON task. See <a href="@cron_settings">CRON Settings</a> for more details.

You can additionally enable notification emails for your customers to inform them whenever a recurring transaction succeeded or failed using the **Recurring Transaction Successful** and **Recurring Transaction Failed** dropdowns in the **Customer notifications** section.

<img src="/doc/squareup/img/recurring_payments.jpg" alt="recurring_payments">