# Demo Page

We welcome you to play around with the Square extension to get acquainted with its behavior.

Feel free to click around [the demo here](#). You will find Square in **Extensions > Payment > Square > Edit**

<p class="bg-info text-info note"><span>Note</span> Some action buttons on the demo website are disabled for security reasons.</p>