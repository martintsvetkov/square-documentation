# Installation & Update Instructions for OpenCart 2.x

<p class="bg-warning text-warning note"><span>Before you begin</span> We recommend you do a full backup of your OpenCart files and database, just in case.</p>

### Preliminary Information

#### All OpenCart releases

After installation/update the following new database tables will be introduced:
- **squareup_customer**: This holds the relationships between your registered customers and their payment information on Square.
- **squareup_transaction**: This holds all transactions created by the extension.
- **squareup_token**: This holds the relationships between your registered customers and their credit card tokens.

#### OpenCart 2.0.0.0 - 2.3.0.2

The release for OpenCart 2.0.0.0 - 2.3.0.2 relies on a modification.

The modification will be applied for the following OpenCart files:

- **catalog/controller/account/account.php**: Loads Square's language file for displaying the extension's account page additions.
- **catalog/controller/{extension/module/,module/}account.php**: Loads Square's language file for displaying the extension's account page additions.
- **catalog/view/theme/\*/template/account/account.tpl**: Inserts the "Your Stored Cards" link in the user's main account page.
- **catalog/view/theme/\*/template/{extension/module/,module/}account.tpl**: Inserts the "Your Stored Cards" in the user's account page sidebar.
- **admin/controller/common/dashboard.php**: Injects a link into the dashboard to show a token expiration message.
- **admin/view/template/common/dashboard.tpl**: Injects an Ajax call into the dashboard to check whether Square's access token is about to expire.
- **admin/controller/sale/order.php**: Injects a link to the order info in the order page.
- **admin/view/template/sale/order_info.tpl**: Inserts a link to the Square transaction view.

### Install using the OpenCart Extension Installer

1. Unzip the downloaded Square .ZIP file into a new directory
2. In your OpenCart admin panel go to **Extensions &gt; Extension Installer**
3. Upload the file <strong>squareup.ocmod.zip</strong> which is in the directory you created in step 1.
<p>If you receive an error "Could not connect as..." this means you have not configured FTP credentials for your Extension Installer. You can resolve this in two ways:<br />Approach 1: Configure FTP from <strong>System &gt; Settings &gt; FTP</strong><br />Approach 2: Install the <a href="http://www.opencart.com/index.php?route=extension/extension/info&extension_id=18892">iSenseLabs QuickFix</a>
When you are done, try uploading <strong>squareup.ocmod.zip</strong> once again.</p>
4. Go to **Extensions > Payments > Square** and click the **Install** button

Congratulations! Square is now installed.

### Manual Installation

1. Unzip the downloaded Square .ZIP file into a new directory
2. Navigate to this directory and find the file <strong>squareup.ocmod.zip</strong>
3. Extract <strong>squareup.ocmod.zip</strong> into a new directory
4. Navigate to the newly extracted directory. You will notice it contains a directory called **upload/**
5. *(Optional)* If necessary, rename the **admin/** and **admin/language/english** directories to match your OpenCart setup
6. Upload the contents of the **upload/** directory to your OpenCart installation, making sure to preserve the directory structure

Congratulations! Square is now installed.

### Update Instructions

<p class="bg-warning text-warning note"><span>Before you begin</span> We recommend you do a full backup of your OpenCart files and database, just in case.</p>

1. Follow one of the above installation methods
2. Go to **Extensions > Payments > Square > Edit** and configure your settings
3. Click **Save** on the top right

That's it! Square is now updated.