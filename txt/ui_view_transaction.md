# User Interface: View Transaction

To view a specific transaction go to **Extensions > Payments > Square > Edit > Transactions** and click on the "View" icon of a transaction. You will be redirected to a page containing all relevant transaction data:

- **Transaction ID**: The Square transaction ID. It is a link that leads to the transaction in your Square dashboard.
- **Merchant ID**: The Merchant ID to which this transaction has been made.
- **Order ID**: The Order ID of the transaction. This is a link leading to the detailed order information.
- **Transaction Type**: The type of Square transaction:
	- **AUTHORIZED**: The card transaction has been authorized but not yet captured.
	- **CAPTURED**: The card transaction was authorized and subsequently captured (i.e., completed).
	- **VOIDED**: The card transaction was authorized and subsequently voided (i.e., canceled).
	- **FAILED**: The card transaction failed.
- **Amount**: The monetary amount of this transaction, displayed with a floating-point. Example: **98.00**
	- If the transaction is AUTHORIZED there will be **Capture** and **Void** buttons at the top of the page.
	- If the transaction is CAPTURED there will be a **Refund** button at the top of the page.
- **Billing company**: The company field from the billing address.
- **Billing street**: The street field from the billing address.
- **Billing City**: The city field from the billing address.
- **Billing ZIP**: The post code field from the billing address.
- **Billing Province/State**: The province field from the billing address.
- **Billing Country**: The ISO 3166-1-alpha-2 country code from the billing address.
- **Customer User Agent**: The user agent (browser) of your customer.
- **Customer IP**: The IP address from which this transaction was made.
- **Date Created**: Date and time when transaction was made.