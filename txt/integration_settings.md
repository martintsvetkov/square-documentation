# Integration Settings

Here is how to integrate the Square OpenCart extension with your Merchant Administration.

This section will give you detailed information about how to configure the following integration-specific fields: **Square Application ID**, **OAuth Application secret**, **Sandbox Application ID**, **Sandbox Access Token**, **Square OAuth Redirect URL**

You can also follow along this [instructional video](https://www.youtube.com/watch?v=placeholder):

<p style="text-align: center"><iframe width="560" height="315" src="https://www.youtube.com/embed/placeholder" frameborder="0" allowfullscreen></iframe></p>

<p class="bg-info text-info note"><span>Note</span> It is important to follow all of these recommendations for the extension to work properly.</p>

### Step 1: Create an application in the Square Developer Portal

You will need to have a merchant account with Square in order to setup an application which will integrate with this extension. You can apply for a merchant account at https://squareup.com/signup/

Once you have setup your account and logged into your [Square Dashboard](https://squareup.com/dashboard/), follow these steps to create your application:

1. From the Sidebar, choose **Apps** to go to the [App Marketplace](https://squareup.com/dashboard/apps)
2. In the top right-hand corner, select **My Apps**
3. In the **Square Connect** section, click the **Go to Developer Portal** button which will take you to the [Developer Application Dashboard](https://connect.squareup.com/apps)

	<img src="/doc/squareup/img/integration/create_application_screen.jpg" alt="create_application_screen">

4. Select **+ New Application**
5. Fill in the **Application name**. This name is for you to recognize the application in the developer portal and is not used by the extension.
6. Click the **Create Application** button at the bottom of the page

You will be taken to the **Manage App** screen for your newly created application where you can configure it. If you wish to come back to this screen at a later date, you can find it in your [Square account dashboard](https://squareup.com/dashboard/) by navigating to **Sidebar > Apps > My Apps** and clicking **Manage App** for the application you want to modify.

<img src="/doc/squareup/img/integration/manage_app_screen.jpg" alt="manage_app_screen">

### Step 2: Connect the Square extension to your Square application

Once you have created an application through the Square Developer Portal, you will need to tell it about your store, as well as fill in some details about it in the OpenCart extension settings.

<img src="/doc/squareup/img/integration_settings.jpg" alt="integration_settings">

Filling out the **Square Application ID** field:

1. Login into the [Developer Application Dashboard](https://connect.squareup.com/apps) and select your application.
2. Navigate to the **Credentials** tab.
3. Copy the **Application ID** field from the **Credentials** section.
	
	<img src="/doc/squareup/img/integration/app_id.jpg" alt="application_id_field">

4. Back in your OpenCart payment extension settings, paste it in the **Square Application ID** field.
5. Click the **Save** button or continue editing the extension settings.

Filling out the **OAuth Application Secret** field:

1. Login into the [Developer Application Dashboard](https://connect.squareup.com/apps) and select your application.
2. Navigate to the OAuth tab.
3. Click the **Show Secret** button to reveal the contents of the **Application Secret** field.
4. Copy the **Application Secret** field.

	<img src="/doc/squareup/img/integration/app_secret.jpg" alt="application_secret_field">

5. Back in your OpenCart payment extension settings, paste it in the **OAuth Application Secret** field.
6. Click the **Save** button or continue editing the extension settings.

To tell your Square application about your store:

1. On the settings page of the Square extension, copy the field **Square OAuth Redirect URL**.
2. Login into the [Developer Application Dashboard](https://connect.squareup.com/apps) and select your application.
3. Navigate to the **OAuth** tab.
4. Paste the address you copied into the **Redirect URL** field.

	<img src="/doc/squareup/img/integration/redirect_url.jpg" alt="redirect_url_field">

5. Click the **Save** button at the bottom of the screen.

<p class="bg-info text-info note"><span>Note</span> This extension requires SSL to be setup correctly for your store. If the address for the <strong>Redirect URL</strong> is using the http protocol instead of https, the Square developer portal will refuse to accept it and you will not be able to connect the extension to the application. If you have SSL enabled, make sure you are accessing the OpenCart administration panel over https when copying the <strong>Square OAuth Redirect URL</strong> field and that it is correctly displaying an https address.</p>

Once you have setup the **Redirect URL** in the Square application and filled out the **Square Application ID** and **OAuth Application Secret** in the extension's administration settings, you can click the **Connect** button to initiate the connection. You will be redirected to https://squareup.com/ to authenticate using the credentials for the Square account you used to create your application in Step 1. If you are already logged into your Square account in another tab or browser window you will not be asked to provide credentials, your Square account will be automatically detected, and you will be redirected back to the extension's administration settings. If there are any error messages, make sure you have filled out the **Square Application ID** and **OAuth Application Secret** fields correctly.

If you have previously revoked your Square application's access to your data and are reconnecting the extension, you will be asked to confirm the application's permissions after clicking **Connect** and logging into your Square account.

<img src="/doc/squareup/img/scope_permissions.jpg">

If you ever wish to change the Square account or application that the Square extension is connected to, create your new application as in Step 1, fill in your new **Application ID**, **Application Secret**, **Sandbox Application ID**, **Sandbox Access Token**, and **Redirect URL** as described in Step 2, then click the **Reconnect** button in your OpenCart extension settings.

### Step 3: (*Optional*) Setting up Sandbox Mode for testing the integration

If you wish to test the functionality of the extension without performing real transactions, you can enable sandbox mode. Any transactions completed using this payment method while in sandbox mode will not be perfomed and will not show up in your Square account dashboard.

<img src="/doc/squareup/img/integration/sandbox_settings.jpg" alt="sandbox_settings">

To enable sandbox mode:

1. In your OpenCart extension settings, switch the **Enable sandbox mode** dropdown to **Enabled** to reveal the sandbox mode settings.
2. Login into the [Developer Application Dashboard](https://connect.squareup.com/apps) and select your application.
3. Navigate to the **Credentials** tab.

	<img src="/doc/squareup/img/integration/square_sandbox_fields.jpg" alt="square_sandbox_fields">

4. Copy the **Sandbox Application ID** field from the **Sandbox** section.
5. Back in your OpenCart payment extension settings, paste it in the **Sandbox Application ID** field.
6. In the Developer Application Dashboard, copy the **Sandbox Access Token** from the **Sandbox** section.
7. Back in your OpenCart extension settings, paste it in the **Sandbox Access Token** field.
8. Click the **Save** button.

When you are done testing, you can disable sandbox mode by toggling the **Enable sandbox mode** dropdown back to **Disabled** and saving. Your sandbox credentials will be remembered, so the next time you can simply toggle it to **Enabled** without re-entering them. If you wish, you can enable sandbox mode and enter these credentials before clicking Connect in Step 2.

If you followed all steps above, you should be now finished with the integration settings.

Do not forget to configure the rest of the extension settings. More information about them can be found in the chapter **User Interface** of this documentation.