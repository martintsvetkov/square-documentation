# Uninstallation Instructions for OpenCart 2.x

<p class="bg-warning text-warning note"><span>Before you begin</span> We recommend you do a full backup of your OpenCart files and database, just in case.</p>

If you correctly follow these instructions, no damage will be done to your OpenCart system.

### Uninstallation Instructions

Navigate to **Extensions > Payments** and click **Uninstall** for the extension **Square**. This removes all extension settings and events, and drops the following database tables: **squareup_customer**, **squareup_transaction**, **squareup_token** meaning all information about stored transactions and saved cards will be lost.

That's all! If you did not like the Square extension for OpenCart, we would love to hear your feedback so we can make it better. Feel free to leave your comments on <a href="mailto:sales@isenselabs.com">sales@isenselabs.com</a>