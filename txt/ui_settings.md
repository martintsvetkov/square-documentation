# User Interface: Settings

This tab allows you to fine-tune the behavior of the Square extension. It holds the following options:

### General Settings

- **Status**: Enable or Disable the Square extension
- **Display Name**: This text is the name of the payment method your customers will see during checkout. Default: Square.
- **Square app redirect URI**: You cannot modify this setting; it is provided to help you setup your Square application. Please refer to the <a href="@integration_settings">Integration Settings</a> page for more information.
- **Square Application ID**: Your Square application ID, located in your Square dashboard. Please refer to the <a href="@integration_settings">Integration Settings</a> page for more information.
- **OAuth Application Secret**: The authentication secret associated with your Square application, located in your Square dashboard. Please refer to the <a href="@integration_settings">Integration Settings</a> page for more information.
- **Transaction type**: This setting controls whether transactions are carried out automatically upon checkout or whether they require manual capture. Selecting **Sale** will result in charges being done automatically, while **Authorize** will result in the store only authorizing the charge, and transactions will need to be captured manually.
- **Total**: The checkout total the order must reach before this payment method becomes active.
- **Geo Zone**: Specify the geo zone for which this payment method will be active.
- **Sort Order**: The sort order of this payment extension relative to your other active payment extensions.
- **Debug Logging**: Use this only for debugging purposes. Enabling this will log the following to your OpenCart error log: notification data, REST API requests, REST API responses.
- **Enable sandbox mode**: This settings controls whether the extension should operate in a testing mode, in which no transactions will be carried out and test card numbers will be accepted. Disabling this setting will hide the additional sandbox settings.
- **Sandbox Application ID**: Your Square application sandbox ID, located in your Square dashboard. Please refer to the <a href="@integration_settings">Integration Settings</a> page for more information.
- **Sandbox Access Token**: The Square sandbox application token, located in your Square dashbord. Please refer to the <a href="@integration_settings">Integration Settings</a> page for more information.

### Merchant Information

- **Merchant name**: You cannot modify this setting; it is provided for you to verify that you have connected the extension to the Square account you inteded to.
- **Access token expires**: You cannot modify this setting; it is provided to let you know when the access token used by the extension to make authenticated calls to Square will expire. If you have not enabled automatic renewal of this token with the CRON job, you will see a warning 5 days before it expires and will have to refresh it manually.
- **Location**: This setting allows you to select which location from your Square account should be used for all transactions. The extension will not work if you have not setup any locations capable of online card processing.

### Transaction Statuses

Any of these statuses can be assigned to your Square transactions. When a transaction for an order is received you will get an Order History entry and the status of the whole order will be updated automatically according to the value you set here:

- **Authorized**: The card transaction has been authorized but not yet captured.
- **Captured**: The card transaction was authorized and subsequently captured (i.e., completed).
- **Voided**: The card transaction was authorized and subsequently voided (i.e., canceled).
- **Failed**: The card transaction failed.

<img src="/doc/squareup/img/order_history.jpg" alt="order_history">

