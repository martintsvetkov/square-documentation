# Software Requirements

Your system must meet the following conditions in order for the extension to work properly:

- OpenCart 1.5.x - OpenCart 2.3.0.2
- SSL installed and enabled for the OpenCart routes:
	- checkout/checkout
	- extension/payment/squareup/oauth_callback (for OpenCart 2.3.x+)
	- payment/squareup/oauth_callback (for OpenCart 1.5.x - 2.2.x)
- vQmod (only for OpenCart 1.5.x)
- PHP 5.4+
- CRON jobs

You will also need a Square Merchant account. If you don't already have one, you can apply here: https://squareup.com/signup/