# Installation & Update Instructions for OpenCart 1.5.x

<p class="bg-warning text-warning note"><span>Before you begin</span> We recommend you do a full backup of your OpenCart files and database, just in case.</p>

### Preliminary Information

The release for OpenCart 1.5.x relies on vQmod. Make sure you have it installed on your store. If not, please follow these instructions: https://www.youtube.com/watch?v=ezS1jWoMmjc

The vQmod file will modify the following OpenCart files:
- **admin/view/template/common/header.tpl**: Used to properly display the SquareUp admin panel.
- **system/startup.php**: Registers an autoloader for the SquareUp library files.
- **catalog/view/theme/\*/template/account/account.tpl**: Inserts the "Your Stored Cards" link in the user's main account page.
- **catalog/controller/account/account.php**: Loads Square's language file for displaying the extension's account page additions.
- **catalog/view/theme/\*/template/module/account.tpl**: Inserts the "Your Stored Cards" in the user's account page sidebar.
- **catalog/controller/module/account.php**: Loads Square's language file for displaying the extension's account page additions.
- **admin/controller/common/home.php**: Injects a link into the dashboard to show a token expiration message.
- **admin/view/template/common/home.tpl**: Injects an Ajax call into the dashboard to check whether Square's access token is about to expire.
- **admin/controller/sale/order.php**: Injects a link to the order info in the order page.
- **admin/view/template/sale/order_info.tpl**: Inserts a link to the Square transaction view.

After installation/update the following new database tables will be introduced:
- **squareup_customer**: This holds the relationships between your registered customers and their payment information on Square.
- **squareup_transaction**: This holds all transactions created by the extension.
- **squareup_token**: This holds the relationships between your registered customers and their credit card tokens.

### Installation Instructions

1. Unzip the downloaded SquareUp .ZIP file into a new directory
2. Navigate to the newly extracted directory. You will notice it contains a directory called **Upload/**
3. *(Optional)* If necessary, rename the **admin/** and **admin/language/english** directories to match your OpenCart setup
4. Upload the contents of the **Upload/** directory to your OpenCart installation, making sure to preserve the directory structure
5. Go to **Extensions > Payments > SquareUp** and click the **Install** button

Congratulations! SquareUp is now installed.

### Update Instructions

<p class="bg-warning text-warning note"><span>Before you begin</span> We recommend you do a full backup of your OpenCart files and database, just in case.</p>

1. Follow the installation instructions above
2. Go to **Extensions > Payments > SquareUp > Edit** and configure your settings
3. Click **Save** on the top right

That's it! SquareUp is now updated.