# Frequently Asked Questions

---

<a name="q1"></a>
### Which cards does the Square extension support?

Square supports any card bearing a Visa, MasterCard, American Express, Discover, JCB, or UnionPay logo, including credit, corporate, debit (processed like credit), prepaid and rewards cards. For more information, visit the [Square Help Website](https://squareup.com/help/us/en/article/5085-accepted-cards).

---

<a name="q2"></a>
### What processing fees are applied to transactions?

Square applies their normal ecommerce rate of 2.9% + $0.30 to manually keyed-in and Card on File payments. For up-to-date information see the official [Square Fees and Payments FAQ](https://squareup.com/help/us/en/article/6109-fees-and-payments-faqs). If your merchant account processes more than $250,000 USD, please contact the [Square Sales department](https://squareup.com/sales/contact).

---

<a name="q3"></a>
### Can I enable recurring payments without using a CRON job?

Yes, you will have to setup a remote CRON service. These services periodically visit a URL on your site to run a scheduled task. See Method 2 in <a href="@cron_settings">CRON Settings</a> for information on how to set that up.

---