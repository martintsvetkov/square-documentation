# User Interface: Transactions

Here is how the tab **Transactions** looks like:

<img src="/doc/squareup/img/transactions.jpg" alt="transactions">

All transactions processed by your Square extension are listed here. The table holds the following columns:

- **Transaction ID**: The Square transaction ID, which matches the one in your Square dashboard.
- **Customer**: The name of the OpenCart customer who performed the transaction.
- **Order ID**: The Order ID of the transaction. This is a link leading to the detailed order information.
- **Type**: The type of Square transaction:
	- **AUTHORIZED**: The card transaction has been authorized but not yet captured.
	- **CAPTURED**: The card transaction was authorized and subsequently captured (i.e., completed).
	- **VOIDED**: The card transaction was authorized and subsequently voided (i.e., canceled).
	- **FAILED**: The card transaction failed.
- **Amount**: The transaction amount.
- **Refunds**: The number of refunds that were applied to this transaction.
- **IP**: The IP address from which this transaction was made.
- **Date Created**: Date and time when transaction was made.
- **Action**: Quick action buttons for viewing more information, capturing, voiding and refunding transactions.

